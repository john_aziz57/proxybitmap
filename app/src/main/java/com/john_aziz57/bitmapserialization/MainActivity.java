package com.john_aziz57.bitmapserialization;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class MainActivity extends ActionBarActivity {

    private static final int SELECT_IMAGE = 0;
    private static final int SELECT_SERIALIZABLE_FILE = 1;
    private Bitmap bitmapImage = null;
    private Bitmap bitmapLoaded = null;
    private String currentImagePath ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * load Image from Gallery
     * @param view
     */
    public void onSelectImageClicked(View view){
        Intent intent =  new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"),SELECT_IMAGE);
    }

    /**
     * saves the image as serializable to the following directory
     * @param view
     */
    public void onSaveClicked(View view){
        ProxyBitmap proxyBitmap = new ProxyBitmap(bitmapImage);
        try {
            File file =new File(currentImagePath+".seri");
            FileOutputStream fout = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(proxyBitmap);
            oos.flush();
            oos.close();
        }catch (FileNotFoundException e){
            // this path is not valid to save
        }catch( IOException e){
            // something wrong with ObjectOutputStream
        }
    }

    /**
     * load Serialized content
     * @param view
     */
    public void onLoadClicked(View view){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, SELECT_SERIALIZABLE_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case SELECT_IMAGE:
                if(resultCode==RESULT_OK){
                    Uri selectedImageUri = data.getData();
                    String[] projection = { MediaStore.MediaColumns.DATA };
                    Cursor cursor = managedQuery(selectedImageUri, projection, null,
                            null, null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    cursor.moveToFirst();
                    currentImagePath = cursor.getString(column_index);
                     bitmapImage = BitmapFactory.decodeFile(currentImagePath);
                    ImageView imageView = (ImageView ) findViewById(R.id.bitmapToSave);
                    imageView.setImageBitmap(bitmapImage);
                }
                break;
            case SELECT_SERIALIZABLE_FILE:
                if(resultCode == RESULT_OK){
                    try {
                        Uri selectedImageUri = data.getData();
                        String[] projection = { MediaStore.MediaColumns.DATA };
                        Cursor cursor = managedQuery(selectedImageUri, projection, null,
                                null, null);
                        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                        cursor.moveToFirst();
                        FileInputStream fIn = new FileInputStream(new File(cursor.getString(column_index)));
                        ObjectInputStream ois = new ObjectInputStream(fIn);
                        bitmapLoaded = ((ProxyBitmap)ois.readObject()).getBitmap();
                        ois.close();
                        ImageView imageView = (ImageView) findViewById(R.id.bitmapToLoad);
                        imageView.setImageBitmap(bitmapLoaded);
                    }
                    catch(IOException e){
                        e.printStackTrace();
                    }catch(ClassNotFoundException e1){
                        e1.printStackTrace();
                    }
                }
        }
    }
}
